# zig-pixman

[zig](https://ziglang.org/) 0.11 bindings for
[pixman](https://gitlab.freedesktop.org/pixman/pixman) that are a little
nicer to use than the output of `zig translate-c`.
